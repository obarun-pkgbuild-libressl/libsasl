# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/core/x86_64/libarchive/
## Maintainer : Jan de Groot <jgc@archlinux.org>
##
## This package spans multiple repositories.
## Always build from cyrus-sasl/trunk and merge changes to libsasl/trunk.
#--------------------------------------------------------------------------------------

pkgname=libsasl
pkgver=2.1.27
pkgrel=2
arch=('x86_64')
license=('custom')
_website="http://cyrusimap.web.cmu.edu/"
pkgdesc="Cyrus Simple Authentication Service Layer (SASL) library"
url="https://github.com/cyrusimap/cyrus-sasl"
source=("$url/releases/download/cyrus-sasl-2.1.27/cyrus-sasl-$pkgver.tar.gz"
    '0003-Update-saslauthd.conf-location-in-documentation.patch'
    '0006-Enable-autoconf-maintainer-mode.patch'
    '0010-Update-required-libraries-when-ld-as-needed-is-used.patch'
    '0013-Don-t-use-la-files-for-opening-plugins.patch'
    '0020-Restore-LIBS-after-checking-gss_inquire_sec_context_.patch'
    '0022-Fix-keytab-option-for-MIT-Kerberos.patch'
    '0032-Add-with_pgsql-include-postgresql-to-include-path.patch'
    'gdbm-errno.patch'
    'saslauthd.conf.d'
    'libsasl.tmpfiles.conf')

options=(
    '!makeflags')

makedepends=(
    'postgresql-libs'
    'mariadb-libs'
    'krb5'
    'libressl'
    'sqlite')

#--------------------------------------------------------------------------------------
prepare() {
    cd "cyrus-sasl-$pkgver"

    patch -Np1 -i ../0003-Update-saslauthd.conf-location-in-documentation.patch
    patch -Np1 -i ../0006-Enable-autoconf-maintainer-mode.patch
    patch -Np1 -i ../0010-Update-required-libraries-when-ld-as-needed-is-used.patch
    patch -Np1 -i ../0013-Don-t-use-la-files-for-opening-plugins.patc
    patch -Np1 -i ../0020-Restore-LIBS-after-checking-gss_inquire_sec_context_.patch
    patch -Np1 -i ../0022-Fix-keytab-option-for-MIT-Kerberos.patch
    patch -Np1 -i ../0032-Add-with_pgsql-include-postgresql-to-include-path.patch
    patch -Np1 -i ../gdbm-errno.patch

    cp -a saslauthd/saslauthd.mdoc saslauthd/saslauthd.8
}

build() {
    export CFLAGS="$CFLAGS -fPIC"
    cd "cyrus-sasl-$pkgver"

    rm -f config/config.guess config/config.sub
    rm -f config/ltconfig config/ltmain.sh config/libtool.m4
    rm -fr autom4te.cache

    libtoolize -c
    aclocal -I config
    automake -a -c
    autoheader
    autoconf

    ./configure                                              \
        --prefix=/usr                                        \
        --sbin=/usr/bin                                      \
        --infodir=/usr/share/info                            \
        --mandir=/usr/share/man                              \
        --sysconfdir=/etc                                    \
        --enable-alwaystrue                                  \
        --enable-anon                                        \
        --enable-auth-sasldb                                 \
        --enable-checkapop                                   \
        --enable-cram                                        \
        --enable-digest                                      \
        --enable-gssapi                                      \
        --enable-login                                       \
        --enable-ntlm                                        \
        --enable-plain                                       \
        --enable-shared                                      \
        --enable-sql                                         \
        --disable-krb4                                       \
        --disable-macos-framework                            \
        --disable-otp                                        \
        --disable-passdss                                    \
        --disable-srp                                        \
        --disable-srp-setpass                                \
        --disable-static                                     \
        --with-configdir=/etc/sasl2:/etc/sasl:/usr/lib/sasl2 \
        --with-dblib=gdbm                                    \
        --with-devrandom=/dev/urandom                        \
        --with-mysql=/usr                                    \
        --with-pam                                           \
        --with-pgsql=/usr/lib                                \
        --with-saslauthd=/var/run/saslauthd                  \
        --with-sqlite3=/usr/lib
        sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
    make
}

package_libsasl() {
    pkgdesc="Cyrus Simple Authentication Service Layer (SASL) Library"
    depends=(
        'libressl')
    conflicts=(
        'cyrus-sasl-plugins')

    cd "cyrus-sasl-$pkgver"

    make DESTDIR="$pkgdir" install-pkgconfigDATA
    for dir in include lib sasldb plugins utils; do
     pushd ${dir}
     make DESTDIR="${pkgdir}" install
     popd
    done

    rm -f "$pkgdir"/usr/lib/sasl2/libsql.so*
    rm -f "$pkgdir"/usr/lib/sasl2/libgssapiv2.so*
    rm -f "$pkgdir"/usr/lib/sasl2/libldapdb.so*
    rm -f "$pkgdir"/usr/lib/sasl2/libgs2.so*

    install -m755 -d "$pkgdir/usr/share/licenses/libsasl"
    install -m644 COPYING "$pkgdir/usr/share/licenses/libsasl/"
}

package_cyrus-sasl() {
    pkgdesc="Cyrus saslauthd SASL authentication daemon"
    depends=(
        "libsasl=$pkgver"
        'krb5'
        'pam')
    backup=(
        'etc/conf.d/saslauthd')

    cd "cyrus-sasl-$pkgver/saslauthd"

    make DESTDIR="$pkgdir" install

    install -Dm644 "$srcdir/saslauthd.conf.d" "$pkgdir/etc/conf.d/saslauthd"
    install -Dm644 "$srcdir/libsasl.tmpfiles.conf" "$pkgdir/usr/lib/tmpfiles.d/saslauthd.conf"

    install -m755 -d "$pkgdir/usr/share/licenses/cyrus-sasl"
    ln -sf ../libsasl/COPYING "$pkgdir/usr/share/licenses/cyrus-sasl/"
}

package_cyrus-sasl-gssapi() {
    pkgdesc="GSSAPI authentication mechanism for Cyrus SASL"
    depends=(
        "libsasl=$pkgver"
        'krb5')
    replaces=(
        'cyrus-sasl-plugins')

    cd "cyrus-sasl-$pkgver/plugins"

    install -m755 -d "$pkgdir/usr/lib/sasl2"
    cp -a .libs/libgssapiv2.so* "$pkgdir/usr/lib/sasl2/"
    cp -a .libs/libgs2.so* "$pkgdir/usr/lib/sasl2/"

    install -m755 -d "$pkgdir/usr/share/licenses/cyrus-sasl-gssapi"
    ln -sf ../libsasl/COPYING "$pkgdir/usr/share/licenses/cyrus-sasl-gssapi/"
}

#package_cyrus-sasl-ldap() {
#   pkgdesc="ldapdb auxprop module for Cyrus SASL"
#   depends=(
#       "libsasl=$pkgver"
#       'libldap')
#   replaces=(
#       'cyrus-sasl-plugins')
#
#   cd "cyrus-sasl-$pkgver/plugins"
#
#   install -m755 -d "$pkgdir/usr/lib/sasl2"
#   cp -a .libs/libldapdb.so* "$pkgdir/usr/lib/sasl2/"
#
#   install -m755 -d "$pkgdir/usr/share/licenses/cyrus-sasl-ldap"
#   ln -sf ../libsasl/COPYING "$pkgdir/usr/share/licenses/cyrus-sasl-ldap/"
#}

package_cyrus-sasl-sql() {
    pkgdesc="SQL auxprop module for Cyrus SASL"
    depends=(
        "libsasl=$pkgver"
        'postgresql-libs'
        'mariadb-libs'
        'sqlite')
    replaces=(
        'cyrus-sasl-plugins')

    cd "cyrus-sasl-$pkgver/plugins"
    install -m755 -d "$pkgdir/usr/lib/sasl2"
    cp -a .libs/libsql.so* "$pkgdir/usr/lib/sasl2/"

    install -m755 -d "$pkgdir/usr/share/licenses/cyrus-sasl-sql"
    ln -sf ../libsasl/COPYING "$pkgdir/usr/share/licenses/cyrus-sasl-sql/"
}

#--------------------------------------------------------------------------------------
sha512sums=('d11549a99b3b06af79fc62d5478dba3305d7e7cc0824f4b91f0d2638daafbe940623eab235f85af9be38dcf5d42fc131db531c177040a85187aee5096b8df63b'
            'ccd8a9e06578a4811d24c883e45d4dc51a35c2875851ab372113d612d9e52470faa08748e8c27b5bdfe8aee47caf91c40b04211ddff43087f7e0b43c5819f783'
            '852791cadfb1bdde948814185f307bbada781f8562319c90fea7d5248d545432e6103cb113dfc89525c8a74cc30f1b8686e52a8b658efad9738609473a1dbc74'
            'e44c7bc90b9140f3cc442c2155ac89c13b9bd5422c2d8ac1fddf83b64c312ba621322b32639c4f517aa05c78d6a1cd94dc257892e706844f6be095a3863127d7'
            'dc056e74d895e81494a39ba05ec97623f9bbd7b61f44ff0bae8cb847c8e5882d47d384d0da429ab0de2c2bd4530b6ead2e595d6dd17960bfc1aa3d2ded0a823d'
            'cf747487df71c66d22e78ec3ca658f2f2752b224851d23070374119378e4a738996eb8256583437f92f34e63488c5a205a8a37deeb8d98bdbe0a679cbb00d8fb'
            '1364657633a4aa2944415477108d38acc94f37ad92470205fb36b7307e4924b02d0029bab4bae4439a2d13cb57d9209974dbf7b55f4cde2448571ccec8d62f9b'
            '48f61c050e8b372b994bc2d56d04a29a07922d2f149ad890f6895e6ec0ed0e7cd2069e63a6445cfe87be23ae95b9ff259b3d70f64a449ceb420587ef21e2f57b'
            'd7dfdf520d16a79f265708d1c6938bd24bd26b9a0ff9b7fcbfc95c494af7f44220080bd3f79d0486bb6fc30b4a9a269adb7836bc593eacca99a1ef549ce58a9e'
            'e99e2da452d2d5b9c0fc7ef10a844f5aa80f20fe5d5778666e450b5e6eb183876322592ae075d55c7e24d93d8a39bf7d47864697faf398cc40fd589808dd0282'
            '2cee9d6626aa16b6b51a5f48ad2c162564ce97dabc367738b4d6af654fcd5845e0d2cd78e5c9fb2378dfba2fd67f32fdbfd42321dc59c9a1222369860312d0a7')
